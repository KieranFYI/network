﻿using System;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {

			Console.Title = "Server";

			using (var server = new Server())
				server.Start();
		}
    }
}
