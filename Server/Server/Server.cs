﻿using Ether.Network.Server;
using System;

namespace Server
{
	internal sealed class Server : NetServer<Client>
	{
		/// <summary>
		/// Creates a new <see cref="Server"/> with a default configuration.
		/// </summary>
		public Server()
		{
			this.Configuration.Backlog = 100;
			this.Configuration.Port = 1861;
			this.Configuration.MaximumNumberOfConnections = 1000;
			this.Configuration.Host = "127.0.0.1";
			this.Configuration.Blocking = true;

			Console.WriteLine("Initiating listener on " + this.Configuration.Host);
		}

		/// <summary>
		/// Initialize the server resources if needed...
		/// </summary>
		protected override void Initialize()
		{
			Console.WriteLine("Server is Listening.");
		}

		/// <summary>
		/// On client connected.
		/// </summary>
		/// <param name="connection"></param>
		protected override void OnClientConnected(Client connection)
		{
			Console.WriteLine("Client connected!");

			connection.SendFirstPacket();
		}

		/// <summary>
		/// On client disconnected.
		/// </summary>
		/// <param name="connection"></param>
		protected override void OnClientDisconnected(Client connection)
		{
			Console.WriteLine("Client disconnected!");
		}

		/// <summary>
		/// On server error.
		/// </summary>
		/// <param name="exception"></param>
		protected override void OnError(Exception exception)
		{
			// TBA
		}
	}
}