﻿using System;
using Ether.Network.Packets;
using System.Linq;
using System.Threading;

namespace Client
{
	internal sealed class Program
	{

		public static bool isRunning = true;

		private static void Main()
		{
			while (isRunning)
			{
				var program = new Program();
				program.Start();
			}
		}

		public void Start()
		{

			Console.WriteLine("Attempting connection...");
			var client = new Client("127.0.0.1", 1861, 512);
			try
			{
				client.Connect();
				while (client.IsConnected)
				{
					NetPacket packet = new NetPacket(1);
					//packet.Write(DateTimeOffset.Now.UtcTicks);
					client.Send(packet);
					Thread.Sleep(500);
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}
			client.Dispose();
		}
	}
}
